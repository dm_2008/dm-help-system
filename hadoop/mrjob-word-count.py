#-*- coding: UTF-8 -*-
from mrjob.job import MRJob
import re

WORD_RE = re.compile(r"[\w'а-яА-Я]+")

class MRJobFrecCount(MRJob):
    def mapper (self, _, line):
        for w in WORD_RE.findall(line):
            #yield (w.lower().encode('utf-8'), 1)
            yield ("line", 1 )
            yield ("word", len(line.split()) )
            yield ("chars", len(line) )

    def combiner (self, key, values):
        yield (key, sum(values) )

    def reducer (self, key, values):
        yield (key, sum(values) )

if __name__ == '__main__':
    MRJobFrecCount.run()

