#!/usr/bin/env python
import sys
(lastKey, sum) = (None, 0)
for line in sys.stdin:
    (key, value) = line.strip().split('	')
    if lastKey and lastKey != key:
        print ('%s	%s' % (lastKey, sum))
        (lastKey, sum) = (key, int(value))
    else:
        (lastKey, sum) = (key, sum + int(value))
if lastKey:
    print ('%s	%s' % (key, sum))
