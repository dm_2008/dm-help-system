#!/usr/bin/env bash
echo "Create Postgres Data Base"

# Создание пользователя 
cd /tmp
nusr="dm2008"
ndb="dm2008db"
sudo -u postgres psql -c "select version();"
sudo su - postgres psql -c "createdb $ndb;"
sudo su - postgres -c "createuser $nusr;"
sudo -u postgres psql -c "alter database $ndb owner to $nusr;"
sudo -u postgres psql -c "grant all privileges on database $ndb to $nusr;"

# Отчет о прослушивании портов
ss -nlt | grep 5432

# Отчет о состоянии сервиса СУБД
systemctl status postgresql.service

# Отчет о доступных БД
sudo -u postgres psql -l

echo "Done."
