#!/usr/bin/env bash
echo "Install PHP 7.2"
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y libpq-dev
DEBIAN_FRONTEND=noninteractive apt-get install -y php7.2
apt-get install -y php7.2-curl

php --version | head -n 1
echo "Done."
