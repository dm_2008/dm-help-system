Данные - Массовая загрузка и анализ
========================================
* Справочная информация 
* Примеры реализации
* Алгоритмы машинного обучения

Оглавление:                                              <a name='toc'></a>

* [__Hadoop__](#hadoop)
    - [__Общие термины и определения__](#hadoop-terms)
* [__Hadoop:HDFS__](#hdfs)
    - [__Примеры работы с hdfs__](#hdfs-example)
    - [__Общие термины и определения__](#hdfs-terms)
    - [__Примеры расчета параметров hdfs__](#hdfs-task)
* [__MapReduce__](#mapr)
    - [__MrJob__](#mr-job)
    - [__Примеры работы с Map Reduce__](#mapr-example)
    - [__Описание процесса обработки Map-Reduce задачи__](#mapr-descr)
    - [__Общие термины и определения__](#mapr-terms)
    - [__Упражнения Map Reduce__](#mapr-task)
* [__Spark__](#spark)
    - [__Общие термины и определения__](#spark-terms)
    - [__Список функций Spark__](#spark-fn-list)
    - [__Базовые примеры работы в Spark__](#spark-example)
    - [__Подсчет количества слов в тексте__](#spark-word-count)
    - [__Самое популярное слово в тексте__](#spark-word-top)
    - [__Разнесение вычислений по нодам__](#spark-paralel)
    - [__Процессы в spark__](#spark-mon)
    - [__Заметки__](#spark-notes)
* [__Kafka__](#kafka)
    - [__Общие термины и определения__](#kafka-terms)
    - [__Примеры работы с Kafka__](#kafka-example)
* [__Git Приемы работы__](#git)
    - [__Git-flow - краткое описание явления__](#git-flow)
    - [__Основные команды GIT__](#git-example)
    - [__Настройка GIT tools__](#git-setup)
    - [__CI-CD - Что это и зачем__](#git-ci-cd)
    - [__CI-CD Примеры__](#cicd-example)
        - Пример .gitlab-ci.yaml
        - Ссылки на внешние примеры
        - Некоторые служебные слова
    - [__Скрипты развертывания тестовых сред__](#env-setup)
        - Установка Postgres
        - Создание базы данных в Postgres
        - Установка php
* [__Контейнеризация данных, стриминг__](#streaming)
    - [__Пример реализации__](#streaming-example)
* [__POSTGRES__](#postgres)
    - [__Postgres - Установка__](#postgres-install)
    - [__Упражнение - Подключение каталога с данными__](#postgres-catalog)
* [__CRISP-DM__](#crisp)
    - [__Общие термины и определения__](#crisp-terms)
    - [__Упражнение__](#crisp-task)
* [__SEMMA__](#semma)
    - [__Общие термины и определения__](#semma-terms)
    - [__Упражнение__](#semma-task)
* [__Agile__](#agile)
    - [__Общие термины и определения__](#agile-terms)
* [__Вспомогательные инструменты__](#assets)
    - [__Vagrant - командная оболочка для virtual box__](#assets-vagrant)
        - [__Vagrant - Основные команды__](#assets-vagrantcmd)
        - [__Vagrant - Подключение по SSH__](#assets-vagrantssh)
        - [__Vagrant - файл настроек__](assets-vagrantfile)
        - [__Vagrant - Создание виртуальной машины__](#assets-vagrantcreateexample)
        - [__Vagrant - Примеры конфигурации__](#assets-vagrantconfig)
    - [__CRON - Планировщик заданий в linux __](#assets-cron)
* [__Алгоритмы машинного обучения__](#algo)
    - [__Общие термины и определения__](#algo-terms)
    - [Основные виды алгоритмов машинного обучения](#algo-types)
    - [~~Примеры реализации~~](#algo-example)
    - [Тестовые наборы данных](#algo-data-sets)
* [Задачи по статистике](#statistics)
    - [Комбинаторика](#stat-combinations)
* [__Паралельная реальность__](#other-tools)
    - [__Postgres__](#postgres)
    - [__Framework Django__](#django)
    - [__Предельтно простой веб сервер__](#http-server)
    - [__Примеры Pandas__](#pandas-example)
    - [__Python Fizz Buz__](#python-fbz)

---


# Hadoop                                              <a name='hadoop'></a>
[К Оглавлению](#toc)

Hadoop - семейство программ, предназначенных для хранения больших объемов
данных в распределенной среде, на нескольких серверах. Основой для всех модулей
системы является система HDFS - распределенное файловой ханилище данных.
Основными дополняющими системами для hdfs можно выделить:
* Spark vs Map Reduce
* HBase

## Spark или Map Reduce...

* Spark - Система, которая хранит промежуточные результаты в оперативной памяти.
* Map Reduce - Промежуточные результаты хранит в hdfs. 

Поэтому, если промежуточные результаты не влазят в оперативку, то расчеты
следует делать на Map Reduce.

## Hadoop: Общие термины и определения                <a name='hadoop-terms'></a>

| Термин             | Описание |
| :---               | :--- |
|                    | **Модули Hadoop** |
| HDFS               | Hadoop Distributed File System. Представляет информацию, которая хранится на отдельных серверах, как единое пространство - сетевой диск. |
| HBase              | Колоночная База данных. Надстройка уже над HDFS, Позволяет выполнять произвольное чтение данных из файла с минимальной задержкой (Low-Latency Reads) |
| Map Reduce         | Система распределенных вычислений, промежуточные результаты пишет на диск, за счет этого тормозит. |
| Spark              | Система распределенных вычислений, промежуточные результаты хранит в оперативной памяти, за счет этого ползает чуть быстрее чем `MapReduce` |
| Zookeeper          | Взаимодействие между нодами |
| Oozie              | Диспетчер задач системы `HDFS`, Позволяет описывать взаимодействие задач `MapReduce` |
| Hive               | Опсание задач MapReduce в стиле SQL|
| Pig                | Опсание задач MapReduce в итеративном стиле |
| Impala             | |
|                    | **Интерфейсы** |
| Direct Access      | Способо взаимодействия с системой HDFS. Клиенты C++, Java|
| Proxy Server Access| Способо взаимодействия с системой HDFS. Серверы Rest, Thrift, Avro |
| middle man         | |
| WebHDFS Rest       | Rest Proxy Server, предоставляет доступ к HDFS через HTTP Запросы. Ответы в форматах JSON, XML, ProtoBuf |
| Avro               | Механизм сериализации при доступе через Proxy Server |
| Thrift             | Язык определения интерфейса, двоичный протоколо связи. Позволяет строить конвееры, вызывать внешние модули на C++, Python, Go и т.д. см: https://ru.wikipedia.org/wiki/Apache_Thrift |
|                    | **Дистрибутивы...** |
| CDH                | Clouder Distribution for Hadoop, Дистрибутив фирмы Cloudera|
| HDP                | Hortonworks Data Platform, Дистрибутив фирмы Hortonworks|
| Apache             | Apache BigTop Distribution, Дистрибутив от самого Apache |
| Greenplum          | Greenplum HD Data Computing Appliance |
| MapR               | MapR Distribution, есть и такая фирма, выпускает свой дистрибутив |

# Hadoop: HDFS                                        <a name='hdfs'></a>

[К Оглавлению](#toc)

HDFS - Распределенная файловая система, где явно выделены узлы данных и
единственный узел имен. Новая реинкарнация файловых серверов, только вид сбоку.

Одна из основных идей системы - устойчивость к
выбыванию узлов. Достигается это за счет дублированя блоков данных на разных
узлах. Поэтому для этой системы не используются RAID-массивы. Она сама по-сути
является квази raid-массивом. 

Вторая основополагающая идея этой системы - это массивное чтение. Система
расчитана на передачу большого объема данных, и не расчитана на множественное
беспорядочное чтение маленьких порций данных.

## Примеры работы с hdfs                            <a name='hdfs-example'></a>
Для работы с файловой системой существует несколько различных вариантов.
* hdfs - консольная программа для работы в bash
* python / jupyter - библиотека [snakebite](https://snakebite.readthedocs.io/en/latest/cli.html)

Блокнот с примерами доступен по [ссылке](/hadoop/hdfs-basics.ipynb).

* Справка
```
$ hdfs -help
...
$ hdfs dfs -help
...
$ hdfs dfs help ls
```


* Список файлов
```
$ hdfs dfs -ls /datasets
> Found 2 items
> -rw-r--r--   1 vagrant supergroup    5589889 2020-01-17 21:52 /datasets/shakespeare-all.txt
> -rw-r--r--   1 vagrant supergroup         83 2020-02-20 20:19 /datasets/short-text.txt
...
$ hdfs dfs -ls -h /datasets
> Found 2 items
> -rw-r--r--   1 vagrant supergroup      5.3 M 2020-01-17 21:52 /datasets/shakespeare-all.txt
> -rw-r--r--   1 vagrant supergroup         83 2020-02-20 20:19 /datasets/short-text.txt
```


* Загрузка файла
```
$ hdfs dfs -put short-text.txt /datasets
```


* Оценка размера директории
```
$ hdfs dfs -df -h /datasets
> Filesystem               Size     Used  Available  Use%
> hdfs://localhost:9000  39.3 G  201.5 M     33.3 G    1%
```


* Удаление файла в корзину
```
$ hdfs dfs -rm /datasets/short-del.txt
> 20/02/20 20:31:51 INFO fs.TrashPolicyDefault: Namenode trash configuration: Deletion interval = 0 minutes, Emptier interval = 0 minutes.
> Deleted /datasets/short-del.txt
```


* Удаление файла на совсем
```
$ hdfs dfs -rm -skipTrash /datasets/short-del.txt
> Deleted /datasets/short-del.txt
```


## Особенности системы HDFS
* Данные дублируются, для возможности восстановления
* Если утерян узел имен, данные потеряны
* Система расчитана на однократную запись
* Один раз записали, много раз прочитали
* Изменение в середине файла не предусмотрено
* Допускается дозапись файлов
* Последовательное чтение файла
* Приспособлена к массивному чтению данных
* Модель использования: "Low Latency Read"
    - высокая пропускная способность
    - низкая скорость доступа к файлам
    - HBase для изменения модели использования
* Размер блока данных ~128mb, настраивается
* Вычисления там где данные, не данные доставляются к коду, а код доставляется к блокам данных
* Работает на кластере из серверов
* Выглядит как единый диск
* Приложение пользовательского уровня (не уровень ядра)
* Хранение больших и очень больших данных
* Количество файлов ограничено, до 10^6 файлов
* Произвольное чтение затруднительно, медленно и неэффективно

## HDFS: Общие термины и определения                <a name='hdfs-terms'></a>

| Термин             | Описание |
| :---               | :--- |
|                    | **Общие термины  HDFS** |
| HDFS               | Hadoop Distributed File System. Представляет информацию, которая хранится на отдельных серверах, как единое пространство - сетевой диск. |
| NameNode           | Узел, где хранится информация о всех файлах системы, где и какие блоки файла находятся |
| Secondary NameNode | Ужимает информацию image на узле NameNode, ускоряет старт сервера после падения |
| NameNode Federation| Актуально для `Hadoop 2.0`. Существует несколько NameNode, Каждая отвечает за часть данных |
| DataNode           | Узел, где хранятся блоки с данными |
| Fault Tolerance    | Характеристика системы - Устойчивость к отказам оборудования |
| Low Latency Read   | Характеристика системы - Система заточена под чтение с низкими задержками по времени |
| Фактор репликации  | Сколько раз будет сохранен один блок файла, избыточность хранения |
| hdfs               | Утилита для работы с распределенной системой. На вход принимает команду и параметры, например: `$hdfs dfs -ls /` |
|                    | **Подсистемы** |
| dfs                | Работа с файловой системой |
| distcp             | Копирование массивных данных между узлами |
| dfsadmin           | Административные задачи |
| balancer           | Перераспределения блоков между серверами|
| fsck               | Проверка состояния файловой системы|
|                    | **Команды подсистемы dfs** |
| -text              | Чтение, и распаковка, файла. Команда подсистемы dfs. |
| -tail              | Чтение хвоста файла. Команда подсистемы dfs. |
| -ls                | Вывод состава каталога. Команда подсистемы dfs. |
|                    | **Обращение к файловым системам** |
| hdfs://            | для обращения к файлам в распределенной системе (hdfs) |
| file://            | для обращения к локальным файлам |

## Shell-Комманды HDFS
Работа с распределенной системой HDFS производится с помощью утилиты hdfs.

Структура комманды: `hdfs <подсистема> -<команда> -<параметры> <URI>`

Где:
* Подсистема - одна из возможных подсистем hdfs 
* Команда - одна из возможных команд подсистемы
* Параметры - допустимые парамтре команды
* URI - полный путь к файлу

Подсистемы:
* dfs       - Общие команды для работы с файловой системой
* distcp    - Массивное паралельное копирование между узлами минуя клиентсую машину
* dfsadmin  - Решение административных задач и настройка системы
* balancer  - Перераспределение блоков данных между узлами данных
* fsck      - Проверка исправности файловой системы

URI (примеры):
* `file:///home/user/some-file.txt` - Локальный путь
* `hdfs://localhost:9000/tmp/some-file.txt` - Путь в системе HDFS
* `/tmp/some-file.txt ` - Путь в системе HDFS (см: fs.default.name)

| Термин             | Описание |
| :---               | :--- |
| hdfs               | Утилита для работы с распределенной системой. На вход принимает команду и параметры, например: `$hdfs dfs -ls /` |
|                    | **Подсистемы** |
| dfs                | Работа с файловой системой |
| distcp             | Копирование массивных данных между узлами |
| dfsadmin           | Административные задачи |
| balancer           | Перераспределения блоков между серверами|
| fsck               | Проверка состояния файловой системы|
|                    | **Команды подсистемы dfs** |
| -text              | Чтение, и распаковка, файла. Команда подсистемы dfs. |
| -tail              | Чтение хвоста файла. Команда подсистемы dfs. |
| -ls                | Вывод состава каталога. Команда подсистемы dfs. |
|                    | **Обращение к файловым системам** |
| hdfs://            | для обращения к файлам в распределенной системе (hdfs) |
| file://            | для обращения к локальным файлам |

Примеры:
```
hdfs dfs -ls hdfs://localhost:9000/path/to/dir
hdfs dfs -ls /path/to/dir/
hdfs dfs -ls file:///home/user/
```


## Примеры расчетов                                 <a name='hdfs-task'></a>
### Скорость чтения (Примеры)
Расчет времени чтения файла размером 100 мб:
* seek time = 10 мс
* transfer rate = 100 МБ/с
* Размер файла: 100 МБ

| размер блока | Время на чтение файла              |
| :---         | :---                               |
| 100          | 100 * 1 + 0.01 * 1 = 100.01 с      |
| 25           | 25  * 4 + 0.01 * 4 = 100.04 с      |
| 1            | 1 * 100 + 0.01 * 100 = 1 + 1 = 2 с |

### Задача 1: "Архитектура HDFS: Оптимальный размер блока"

Какой должен быть размер блока файлов в HDFS (в МБ), чтобы seek time составлял
1,5% от transfer time (считайте, что tranfer time ≈ время чтения блока), если:
* seek time = 20 мс
* transfer rate = 48 МБ/с

Решение:

| Характеристика            | ед  | Вычисление             | Результат | Примечание |
| :---                      | :---| :---                   | :---      | :---       |
| Время поиска блока        | сек | 20 мс / 1000           | 0,02      |            |
| Время поиска блока        | %   | по условию             | 1,5       |            |
| Общее вемя чтения         | сек | 0,02/1,5 *100          | 1,33(3)   |            |
| Размер прочитанных данных | МБ  | 48 * (0,02/1,5 * 100)  | 64        | тут допущение, если без допущений, то надо умножать на 98,5 |
|Ответ|| Размер блока 64мб|


### Задача 2: "Архитектура HDFS: Состав файла"
Сколько блоков в HDFS будет иметь файл размером 1025 Мб?  Характеристики HDFS
следующие:
* Объем HDFS: 15 Пб
* Размер блока: 128 Мб
* Число серверов: 500
* Фактор репликации: 3

Решение:

| Характеристика                          | ед    | Вычисление             | Результат | Примечание |
| :---                                    | :---  | :---                   | :---      | :---       |
| Количество блоков для файла             | шт    | 1025 / 128             | 8.007 = 9 | Округление в большую сторону до целого |
| Количество блоков с учетом дублирования | шт    | 9 * 3                  | 27        |
| Ответ|| 27 блоков|



# MapReduce                                           <a name='mapr'></a>
[К Оглавлению](#toc)

Возможные варианты работы с Map Reduce:
* [Mining of Massive Datasets](http://infolab.stanford.edu/~ullman/mmds/book.pdf)
* [MrJob (Hadoop Streaming)](https://pypi.org/project/mrjob/)
* [Hadoop Py](https://hadoopy.readthedocs.io/en/latest/)
* [PyDoop (Чистый Python, минуя Streaming)](https://crs4.github.io/pydoop/)
* [Scala (пример I)](https://dzone.com/articles/deep-dive-into-apache-flinks-tumblingwindow)
* [Scala (пример II)](https://github.com/deanwampler/scala-hadoop)


## MrJob                                              <a name='mr-job'></a>

Пакет дает некоторую абстракцию и упрощение формирования задачи в одном файле и классе.
Позволяет реализовывать многошаговый конвеер.

В python 2.7 не работает с кирилицей

Установка пакета
```
pip install mrjob
```

Примеры для библиотеки MrJob:
* [Пример раз](https://github.com/Yelp/mrjob/tree/master/mrjob/examples)
* [Пример два](https://www.thomashenson.com/hadoop-python-example/)
* [Документация и примеры](https://mrjob.readthedocs.io/en/latest/guides/writing-mrjobs.html#single-step-jobs)


Для запуска процесса расчета необходимо описать класс
и затем запустить конвеер на выполнение.

## Примеры работы с Map Reduce                        <a name='mapr-example'></a>

### Пример подсчета количества слов (MrJob)

* определение расчетной задачи, файл: [mrjob-word-count.py](./hadoop/mrjob-word-count.py)

```
#-*- coding: UTF-8 -*-
from mrjob.job import MRJob
import re

WORD_RE = re.compile(r"[\w'а-яА-Я]+")

class MRJobFrecCount(MRJob):
    def mapper (self, _, line):
        for w in WORD_RE.findall(line):
            #yield (w.lower().encode('utf-8'), 1)
            yield ("line", 1 )
            yield ("word", len(line.split()) )
            yield ("chars", len(line) )

    def combiner (self, key, values):
        yield (key, sum(values) )

    def reducer (self, key, values):
        yield (key, sum(values) )

if __name__ == '__main__':
    MRJobFrecCount.run()
```




* Запуск конвеера на кластере
```
$ python mrjob-word-count.py hdfs:///datasets/short-text.txt -r hadoop 
...
Streaming final output from hdfs:///user/vagrant/tmp/mrjob/mrjob-word-count.vagrant.20200220.224537.888266/output...
"chars"	436
"line"	16
"word"	48
Removing HDFS temp directory hdfs:///user/vagrant/tmp/mrjob/mrjob-word-count.vagrant.20200220.224537.888266...
Removing temp directory /tmp/mrjob-word-count.vagrant.20200220.224537.888266...
```

* Запуск конвеера локально
```
$ python mrjob-word-count.py ./short-text.txt 
```





### Пример подсчета количества слов (streaming)

* файл фазы map: word-count-map.py
```
#!/usr/bin/env python
import sys
for line in sys.stdin:
    for token in line.strip().split(" "):
        if token:
            print ('%s	1' % token)
```



* файл фазы reduce: word-count-reduce.py
```
#!/usr/bin/env python
import sys
(lastKey, sum) = (None, 0)
for line in sys.stdin:
    (key, value) = line.strip().split('	')
    if lastKey and lastKey != key:
        print ('%s	%s' % (lastKey, sum))
        (lastKey, sum) = (key, int(value))
    else:
        (lastKey, sum) = (key, sum + int(value))
if lastKey:
    print ('%s	%s' % (key, sum))
```



* Запуск конвеера:
```
$ hadoop jar /usr/local/hadoop/share/hadoop/tools/lib/hadoop-streaming-2.6.4.jar \
    -files word-count-map.py,word-count-reduce.py \
    -mapper word-count-map.py \
    -reducer word-count-reduce.py \
    -input /datasets/short-text.txt \
    -output /tmp/wc.out

> packageJobJar: [/tmp/hadoop-unjar2271776778574923400/] [] /tmp/streamjob7572799866017482000.jar tmpDir=null
> 20/02/21 09:02:22 INFO client.RMProxy: Connecting to ResourceManager at /0.0.0.0:8032
> 20/02/21 09:02:22 INFO client.RMProxy: Connecting to ResourceManager at /0.0.0.0:8032
> 20/02/21 09:02:24 INFO mapred.FileInputFormat: Total input paths to process : 1
> 20/02/21 09:02:24 INFO mapreduce.JobSubmitter: number of splits:2
> ...
> 20/02/21 09:03:08 INFO streaming.StreamJob: Output directory: /tmp/wc.out
```

* Проверка результата:
```
$ hdfs dfs -ls /tmp/wc.out
> Found 2 items
> -rw-r--r--   1 vagrant supergroup          0 2020-02-21 09:03 /tmp/wc.out/_SUCCESS
> -rw-r--r--   1 vagrant supergroup        101 2020-02-21 09:03 /tmp/wc.out/part-00000

$hdfs dfs -cat /tmp/wc.out/part-00000
> ...	1
> Мама	2
> Папа	1
> Рама	1
> маму	1
> мыл	1
> мыла	2
> раму	2
> рыдала	1
> тихо	1
```





## Описание процесса обработки Map-Reduce задачи                     <a name='mapr-descr'></a>

Файл разбивается на части. Над каждой частью запускается свой обработчик -
`mapper`. Запущенный процесс называют worker. После обработки всех блоков файла
запускается фаза `reduce`. В случае падение какого-то из процессов mapper
специальный процесс master перезапускает этот обработчик, по завершении фазы
Reduce в HDFS записывается результирующий файл.

Особенности:
* Результат работы mapper пишется на локальный диск
* При необходимости это можно обойти
* Результат работы reducer пишется в HDFS


## MapReduce: Общие термины и определения             <a name='mapr-terms'></a>

* [Тут термины и определения](https://data-flair.training/blogs/hadoop-combiner-tutorial/)

| Термин                | Определение |
| :---                  | :---        |
| map                   | Первая фаза вычислительной задачи, выполняется на узле с данными. Это сюда "доставляется код" в истории не данные к коду а код к данным.                           |
| combine               | Этап подведения промежуточных итогов по результатам работы на этапе mapp, фаза выполняется на том-же узле где и фаза map.                                          |
| reduce                | Финальный этап вычислительной задачи, подведение итогов и вывод результата.                                                                                        |
| partition             | Секция файла, при загрузке в hdfs файл делится на секции. Количество секций можно задать как при загрузке, так и позднее.                                          |
| master                | Координирующий процесс - оркестратор. Перезапускает фазу mapper в случае падения, отвечает за координацию всей истории mapper->file->reducer->file.                |
| split                 | Часть файла, которая отправляется на обработку в отдельный процесс. Обчыно совпадает с блоком в HDFS. Но есть исключение, файлы архивов отправляются на обработку целиком, т.е. один файл - один сплит. |
| master                | Координирующий процесс - оркестратор. Перезапускает фазу mapper в случае падения, отвечает за координацию всей истории mapper->file->reducer->file.                |
| worker                | процесс обработки, может запускать как фазу map, так и фазу reduce, запускается паралельно и независимо.                                                           |
| mapper worker         | Процесс обработки фазы mapper над одной частью файла, сплитом. Количество процессов обычно совпадает с количеством блоков в файле.                                 |
| mapper                | трансформатор данных, фаза первичной обработки данных, промежуточная агрегация.                                                                                    |
| combiner              | Процесс фазы combine, подводит промежуточные итоги от работы процесса mapper, запускается на том же узле где и mapper.                                             |
| reducer               | Процесс финальной агрегации данных.                                                                                                                                |
| reducer worker        | Процесс обработки фазы reducer. Количество reducer процессов определяется в задаче.                                                                                |
| mapper output file    | Результат работы процесса на фазе mapper. Результаты соханяются на локальный диск.                                                                                 |
| reducer output file   | Результат работы процесса на фазе mapper. Результаты соханяются в HDFS.                                                                                            |
| JopTracker            | Головной процесс "оркестратор" во фреймворке Hadoop MapReduce.                    Подробнее см [здесь](https://www.hadoopinrealworld.com/jobtracker-and-tasktracker/)|
| TaskTracker           | Процесс запускается на каждом узле кластера, запускает процессы вычисления задач. Подробнее см [здесь](https://www.hadoopinrealworld.com/jobtracker-and-tasktracker/)|
| data chunnker         | Фрагментатор данных.                                                                                                                                               |
| Hadoop Streaming      | Конвеер stdin-stdout, позволяет запускать вычислительные задачи на всем, что запускается в linux.                                                                  |
| Shufle                | Процесс копирования данных с маперов на редьюсеры. Или, процесс передачи промежуточных данных на reducer.                                                          |
| split                 | когда файл разбивается на части отправляется на обработку в mapper, такая часть называется split                                                                   |
| Data Locality         | Процесс обработки данных там же, где они лежат. Это всё та-же история про доставку кода к данным...                                                                |
| Промежуточные данные  | Промежуточные данные работы mapper хранятся на локальном диске. При необходимости это может быть изменено. Промежуточные данные работы reducer хранятся в HDFS.    |


## Упражнения Map Reduce                                 <a name='mapr-task'></a>
### Задача 1.
Сколько mapper-процессов запустится для обработки файла /data/logs.gz размером 1Гб, если размер блока в HDFS равен 64Мб?

Ответ: 1, файлы архивов не разбиваются на сплиты.

# Spark                                               <a name='spark'></a>
[К Оглавлению](#toc)

[Spakr](https://spark.apache.org) чуть быстрее Map Reduce, по заявлению на сайте,
быстрее в 100 раз.  Но важно помнить, что между бесконечностью и 100
секонечностями разницы нет. Так что от оптимизации не скрыться.

Основное приемущество - промежуточные результаты хранит в оперативной памяти.
Но говорят... что если на промежуточных этапах применять кэширование, то можно
получить повыешние производительности.
TODO: Замеры скорости с методами кэширования и без.

Основной объект, с которым работаее spark - RDD. Это такой объект, который
хранится в памяти и поддерживает два вида операций, преобразования и действия.
Причем преобразования делать он начинает только после команды-действия. Это
называется "ленивое" поведение.

Но при этом концепция такая-же как в MapReduce. Есть операции, которые
выполняются на узлах и эти операции есть операции преобразования. Есть
операции, которые агрегируют полученные значения, эти операции - операции
действия.

* Вычисления на узлах: rdd.map(<function>)
* Агрегация rdd.reduce(<fucntion>)
* Можно собирать в конвеер, одно за другим: rdd.map(<function>).reduce(<function>)

Вот [здесь](https://habr.com/ru/company/piter/blog/276675/) чуть чуть на русском.

Ссылки:
* [Знакомство с Apache Spark (Habr)](https://habr.com/ru/company/piter/blog/276675/)
* [Spark Basics : RDDs,Stages,Tasks and DAG](https://medium.com/@goyalsaurabh66/spark-basics-rdds-stages-tasks-and-dag-8da0f52f0454)
* [Spark Tutorials](https://www.tutorialspoint.com/apache_spark/index.htm)
* [Примеры Apache Spark](https://spark.apache.org/examples.html)
* [PySpark – Краткое руководство](https://coderlessons.com/tutorials/python-technologies/izuchite-pyspark/pyspark-kratkoe-rukovodstvo)
* [Библиотека PySaprk](https://spark.apache.org/docs/latest/api/python/index.html)
* [__Mining of Massive Datasets__](http://infolab.stanford.edu/~ullman/mmds/book.pdf)

## Spark: Общие термины и определения                 <a name='spark-terms'></a>
[К Оглавлению](#toc)

| Термин     | Описание |
| :--        | :--      |
| RDD        | Акроним от "Resiletn Distributed Dataset", отказоустойчивый и распределенный набор данных. |
| DAG        | Акроним от "Directed Acyclic Graph", направленный граф.                                    |
| Parquet    | бинарный формат хранения файлов. Файл имеет типизацию. По сути файл базы данных.           |
| MLib       | Библиотека машинного обучения.                                                             |
| GraphX     | Библиотека обработки графов.                                                               |


## Список функций Spark                                <a name='spark-fn-list'></a>
[К Оглавлению](#toc)

Полный список можно найти здесь: https://spark.apache.org/docs/latest/api/python/pyspark.html#pyspark.RDD

| функция        | параметр на вход | Действие                                         |
| :--            | :--              | :--                                              |
| map            | функция          | Применяет функцию к каждому элементу набора      |
| reduce         | функция          | аггрегирует элементы в RDD наборе                |
| flatMap        | функция          | Из вложенных наборов собирает один линейный      |
| filter         | функция          | Возвращает элементы, где функция сказала true    |
| sample         | сЗаменой, доля, начальноеЧисло | бутстрап выборка                   |
| distinct       |                  | Уникальынй набор                                 |
| coalesce       | числоРазделов    | сокращает до заданного числа разделов            |
| repartition    |                  | Перетасовывает данные по сети                    |
| groupByKey     |                  | создает новый RDD, ключ - последовательность значений |
| reduceByKey    | функция          | агрегация по ключу, агрегирующая функция как аргумент |
| sortByKey      | по_возрастанию   |                                                  |
| union          | другой_RDD       |                                                  |
| intersection   | другой_RDD       |                                                  |
| join           | другой_RDD       |                                                  |
| cartesian      | другой_RDD       |                                                  |
| leftOuterJoin  | другой_RDD       |                                                  |
| rightOuterJoin | другой_RDD       |                                                  |
| fullOuterJoin  | другой_RDD       |                                                  |
| count          |                  | Количесвто элементов в наборе                    |
| countByKey     |                  | Количество элементов для каждого ключа, вовращает словарь |
| collect        |                  |                                                  |
| first          |                  | Один первый элемент                              |
| take           | N                | Первые n элементов набора                        |
| takeSample     | сЗаменой, доля, начальноеЧисло |                                    |
| takeOrdered    | N, keyFunc       | Возвращает n первых элементов. Моно задать функцию определения ключа для сортировки |
| saveAsText     |                  |                                                  |
| cache          |                  |                                                  |
| persist        |                  |                                                  |
| broadcast      | RDD              | Распространение переменной по всем узлам         |
| unpersist      |                  | Освобождение памяти, занятой командой broadcast  |

## Примеры работы в Spark                             <a name='spark-example'></a>
[К Оглавлению](#toc)

Блокнот с примерами доступен по [ссылке](/hadoop/pyspark-basics.ipynb).

Особенности:
* Основной объект операций - RDD
* операции-преобразования (фаза map в MapReduce)
* операции-действия       (фаза reduce в MapReduce)
* вычисления начинаются только после вызова любой операции-действия
* как и Pandas поддерживает lambda функции (ну вернее это питон поддерживает, ну это так)

### Базовые операции с объектами в Spark

* Проверка контекстной переменной (без нее работать не будет)
```
r = sc._conf.getAll()
print ([r[-1], r[5]])
```


* Создание RDD набора
```
n = range(10)
n_rdd = sc.parallelize(n)
```

* Собрать данные на текущем узле (и вывести)
```
n_rdd.collect()
```

* Частичный просмотр данных
```
n_rdd.take(3)
```

* Сохранение набора в файл
```
n_rdd.saveAsTextFile('hdfs:///tmp/numbers_ones.txt')
```


* Сохранение набора в файл с одной секцией
```
n_rdd.coalesce(1).saveAsTextFile('hdfs:///tmp/numbers_ones.txt')
```

### Простейшие вычисления в Spark

* Создание RDD набора
```
n = range(10)
n_rdd = sc.parallelize(n)
```

* Возведение в квадрат всех элементов
```
def sq(x):
    return x**2

n_rdd.map(sq).collect()
> [0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
```


* Сумма квадратов набора (через reduce)
(угу, тут collect не нужен)
```
def sm(a, b):
    return a + b

n_rdd.map(sq).reduce(sm)
```


* Сумма квадратов набора (через reduce)
(внезапно есть спецальный редюсер - sum
```
n_rdd.map(sq).sum()
```

### Преобразование набора в ключ-значение
* Создание RDD набора
```
n = range(10)
n_rdd = sc.parallelize(n)
```

* Метка 
```
def tag(x):
    r=''
    if x % 2 == 0:
        r= 'even'
    else:
        r= 'odd'
    return r

n_rdd.map(tag).take(3)
```


* Ключ значение
```
def ctt(x):
    return (tag(x), x)

n_rdd.map(ctt).take(3)
```



* Сумма по ключу
```
def sm(a, b):
    return a + b

n_rdd.map(ctt).reduceByKey(sm).collect()
```



* Возведение в квадрат и сумма по ключу
```
def sq(x):
    return x**2

n_rdd.map(sq).map(ctt).reduceByKey(sm).collect()
```



### Еще примеры на Spark

* Создать широковещательную переменную
```
a = {'M':(1, 0, 0),
     'F': (0, 1, 0),
     'U': (0, 0, 1)}
bcast_map = sc.broadcast (a)
```


* Прочитать широковещательную переменную
```
b = bcast_map.value
```



* Подсчет количества строк в файле
(без использования аккумулятора)
```
"""
Здесь используются два автономных задания
лучше использовать один проход и аккумуляторную переменую
"""
print ('Количество пустых строк в файле: {0}'
      .format(
         sc.textFile('file:///vagrant/notebooks/short-text.txt')
        .filter(lambda line: len(line) == 0).count()
    )
 )
```

* Подсчет количества строк в файле
( с использованием аккумуляторной переменной, за один проход)
```
""" Переменная аккумулятор, с начальным значением == 0 """
accum = sc.accumulator(0)

def split_line(line):
    """
    Вспомогательная процедура подсчета строк
    Подсчитываем количесвто строк в файле
    если строкапустая, обновляем аккумуляторную переменную
    """
    if len(line) == 0:
        accum.add(1)
    return 1

""" Запуск вычислительного конвеера """
total_lines = (
    sc.textFile('file:///vagrant/notebooks/short-text.txt')
    .map(split_line)
    .count()
)

""" Вывод результата """
empty_lines = accum.value
print ('Всего строк в файле: {0}\nПустых строк в файле: {1}'
      .format(
        total_lines
      , empty_lines
    )
)
```


### Создание контекста (если не задан)

* Установка библиотеки [findspark](https://pypi.org/project/findspark/)
```
$ sudo pip install findspark
```

* Импорт библиотек, findsaprk строго перед pyspark
```
import findspark;
findspark.init()

import pyspark
...
print (sc)
print (spark)
```


## Подсчет количества слов в тексте (Spark)                             <a name='spark-word-count'></a>
[К Оглавлению](#toc)

Блокнот с примерами доступен по [ссылке](/hadoop/pyspark-word-count.ipynb).

* Загрузить набор в RDD
```
textRDD = sc.textFile('hdfs:///datasets/short-text.txt')
```



* Вспомогательная функция, возвращает ключ-значение, принимает строку
```
def map_stat(line):
    return [('chars', len(line))
        , ('words', len(line.split()))
        , ('lines', 1)]
map_stat('Папа читал газету')
textRDD.flatMap(map_stat).take(5)
```


* Вспомогательная функция суммирует значения
```
def sm (a, b):
    return a + b
    sm (2, 4)
```

* Для каждой строки возвращаем пару ключ-значение (в данном случае возвращаются три пары)
```
textRDD.flatMap(map_stat)...
```

* Для каждого ключа производится агрегация
```
textRDD.flatMap(map_stat).reduceByKey(sm)...
```


* Собрать все результаты вместе
```
textRDD.flatMap(map_stat).reduceByKey(sm).collect()
> [('chars', 60), ('lines', 5), ('words', 13)]
```


## Самое популярное слово в тексте (Spark)                             <a name='spark-word-top'></a>
[К Оглавлению](#toc)


Блокнот с примерами доступен по [ссылке](/hadoop/pyspark-word-top.ipynb).

* Загрузить текст в RDD набор
```
textRDD = sc.textFile('hdfs:///datasets/hadoop_git_readme.txt')
```

* Извлечь слова из строки
```
import re

def map_stat(line):
    WR = re.compile(r'[\wа-яА-Я'']+')
    return WR.findall(line.lower())

textRDD.flatMap(map_stat).take(5)
```
* Формирование пары ключ-значение
```
def one_key(key):
    return (key, 1)

textRDD.flatMap(map_stat).map(one_key).take(3)
```

* Агрегирование по ключу
```
def sm (a, b):
    return a + b

textRDD.flatMap(map_stat).map(one_key).reduceByKey(sm).take(3)
```


* Выбор значения для сортировки и извлечение n-первых элементов
```
def extract_sort_key((key,val)):
    return -val

textRDD.flatMap(map_stat).map(one_key).reduceByKey(sm).takeOrdered(1, key=extract_sort_key)
> [(u'the', 27843)]
```

## Разнесение вычислений по нодам                    <a name='spark-paralel'></a>
[К Оглавлению](#toc)

Блокнот с примерами доступен по [ссылке](/hadoop/pyspark-parallel.ipynb).

## Процессы в spark                                  <a name='spark-mon'></a>
[К Оглавлению](#toc)

Spark имеет свой web-монитор состояния процессов.

Порт и IP Адресс выясняем у админов.

* Текущие процессы
```
http://127.0.0.1:8088/cluster
...
ID: application_1582813746330_0013	
```

* Детализация по процессу
```
http://127.0.0.1:8088/proxy/application_1582813746330_0013
...
> DAG Visualization - граф вычислений процесса
```





## Spark: Заметки                                    <a name='spark-mon'></a>

* Collect падает
```
> При использовании Colleсt() 
> весь результат собирается в память в драйвере 
> и он может упасть. 
...
> Обычно данные просто записываются в HDFS. Это ещё один пример преимущества
> распределённой файловой системы: данные из каждой партиции запишутся локально
> и в паралелли. Если же надо получить на выходе ровно один файл, то можно
> вызвать .repartition(1) и тот же .saveAsHadoopFile() — все данные пойдут на
> одну машину, но не в виде одной коллекции, а в виде ленивого итератора, что
> полностью решает проблему с памятью. 
...
> Нет проблемы с памятью — проблемы со скоростью выгрузки данных из RDD на
> диск. Если получать итератор по данным и качать из него — 3 дня ждать
> приходится :-) Repartition может выполняться неразумно долго, пробовали. Я
> код накидал выше, он позволяет выкачивать RDD гораздо быстрее в нашем случае.
https://habr.com/ru/company/bitrix/blog/273279/
```

* Где какие данные хранить (мнение)
```
> Если хотите построить эффективную систему на Hadoop, то складывайте все
> пакетные данные в HDFS, а данные со случайным доступом — в какое-нибудь NoSQL
> хранилище (HBase, Cassandra, Redis — тут уже в зависимости от вариантов
> использования)
```





# Kafka                                               <a name='kafka'></a>
[К Оглавлению](#toc)

Kafka - Очередь, которая(ый) просто работает.
Издатели пишут сообщения, подписчики их читают. 

Картинка из статьи на хабре "Apache Kafka – мой конспект".

<img src="https://hsto.org/webt/g3/lj/mb/g3ljmbhk1e_6ugq_nscpos5xeuk.jpeg" alt="kafka-proccess" width="800"/>

Ссылки:
* [Apache Kafka и потоковая обработка данных с помощью __Spark Streaming__](https://habr.com/ru/post/451160/)
* [Лаконинчное описание на хабре](https://habr.com/ru/post/354486/)
* [Библиотека для работы с очередью на Python](https://kafka-python.readthedocs.io/en/master/index.html)
* [~~Tutorial Kafka~~](https://www.tutorialkart.com/apache-kafka-tutorial/)
* [~~Kafka Console Producer and Consumer~~](https://www.tutorialkart.com/apache-kafka/kafka-console-producer-and-consumer-example/)
* [~~Видео Spark + Kafka~~](https://www.youtube.com/watch?v=f5GxvgyHznM)
* [~~Анонс книги Kafka: The definition guide~~](https://habr.com/ru/company/piter/blog/352978/)
* [~~Анонс книги Spark + Kafka~~](https://habr.com/ru/company/piter/blog/417123/)

## Kafka: Общие термины и определения                <a name='kafka-terms'>

| Термин                | Определение                      |
| :---                  | :---                             |
| topic                 | Тема. Имя очереди. Подписчик отсюда читает, а издатель сюда пишет. |
| consumer              | Подписчик. Процесс, который вычитывает сообщения из очереди. |
| producer              | Издатель. Процесс, который публикуте сообщения. |
| group                 | Группа. Подписчиков можно поделить на группы. |
| partition             | Раздел. Для хранения сообщений можно настроить несколько разделов. |
| streams               | Потоки.                          |
| offset                | Смещение. Указатель на последнее прочитанное сообщение. |
| Zookeeper             | Координатор, обязательный компонент системы. А без него кафка не работает     |


## Примеры работы с Kafka                            <a name='kafka-example'>

* Про установку можно прочитать [здесь](https://habr.com/ru/post/451160/)

* Создание очереди "transactions"
```
$ bin/kafka-topics.sh \
        --create \
        --zookeeper localhost:2181 \
        --replication-factor 1 \
        --partitions 3 \
        --topic transaction
```

* Просмотр активных очередей:
```
$ bin/kafka-topics.sh --describe --zookeeper localhost:2181
```




# Алгоритмы машинного обучения                        <a name='algo'></a>
[К Оглавлению](#toc)

## Общие термины и определения                        <a name='algo-terms'></a>

| Термин           | Описание |
| :--- | :--- |
| ML | От английского "Machine learning" - Машинное обучение |
| NN, ANN | от английского "Artificial Neural Networks" - Искуственная Нейронная сеть |
| фичи | От английского "feachures" - свойства, некоторый набор переменных, с которыми предстоит работать |

### Машинное обучение
Очень расплывчатй термин, который по сути означает - "Математическая
статистика".  Очень мало реальных разделов мат. статистики, которые можно
вчистую отнести именно к разделу машинного обучения, т.е. там где машины учатся
сами.  Пожалуй я могу выделить лишь одну область - это Обучение с подкреплением
(Reinforcement Learning).  Остальное было известно и раньше, просто мат.стат
это скучно, а _машинное обучение_ - это модно и круто.


## Основные виды алгоритмов машинного обучения        <a name='algo-types'></a>

Классификация алгоритмов машинного обучения дело неблагодарное. Они имеют
множество свойств, которые несравнимы между собой. Например Нейросетевые
алгоритмы можно выделить в отдельную группу, но в зависимости от архитектуры,
конкретные реализации можно отнести и к обучению с учителем и к генеративным
сетям, и даже к обучению с подкреплением.  Хотя в случае обучения с
подкреплением нейросеть все-таки применяется как и в варианте обучения с
учителем.

И так,не претендуя на полноту классификации можно выделить следующие типы алгоритмов:
* Обучение с Учителем
* Обучение без учителя
* Обучение с подкреплением
* Ансамбли
* Нейронные сети

По области применения:
* Классификация
* Кластеризация
* Регрессия
* Уменьшение размерности
* Ассоциация
* Нейронные сети (опять)
* Генеративные нейросети
* Авто-кодировщики (нейросети)
* Непосредственно машинное обучение

Примерный список алгоритмов ML:

| Название алгоритма                                 | Область применения         | Тип алгоритма                |
| :---                                               | :---                       | :---                         |
| 01.Наивный Байес                                   | Классификация              | Обучение с учителем          |
| 02.Деревья решений                                 | Классификация              | Обучение с учителем          |
| 03.Логистическая регрессия                         | Классификация              | Обучение с учителем          |
| 04.К-ближайших соседей                             | Классификация              | Обучение с учителем          |
| 05.Машина (метод) опорных векторов                 | Классификация              | Обучение с учителем          |
| 06.Линейная                                        | Регрессия                  | Обучение с учителем          |
| 07.Полиноминальная                                 | Регрессия                  | Обучение с учителем          |
| 08.Криволинейна                                    | Регрессия                  | Обучение с учителем          |
| 09.Rige/Lasso                                      | Регрессия                  | Обучение с учителем          |
| 10.DBSCAN                                          | Кластеризация              | Обучение без учителя         |
| 11.Метод К-Средних                                 | Кластеризация              | Обучение без учителя         |
| 12.Сдвиг среднего значения, Mean-Shift             | Кластеризация              | Обучение без учителя         |
| 13.Метод главных компонент (PCA)                   | Уменьшение размерности     | Обучение без учителя         |
| 14.Метод независимых компонент (ICA)               | Уменьшение размерности     | Обучение без учителя         |
| 15.Латентное разложение Дирихле (LDA)              | Уменьшение размерности     | Обучение без учителя         |
| 16.Латентно-семантический анализ (LSA, pLSA, GLSA) | Уменьшение размерности     | Обучение без учителя         |
| 17.Сингулярное разложение (SVD)                    | Уменьшение размерности     | Обучение без учителя         |
| 18.Нечто для визуализации (t-SNE)                  | Уменьшение размерности     | Обучение без учителя         |
| 19.Apriori                                         | Ассоциация                 | Обучение без учителя         |
| 20.ELCAT                                           | Ассоциация                 | Обучение без учителя         |
| 21.FP-Роста                                        | Ассоциация                 | Обучение без учителя         |
| 22.Процедура ASSOC метода GUHA                     | Ассоциация                 | Обучение без учителя         |
| 23.Поиск OPUS                                      | Ассоциация                 | Обучение без учителя         |
| 24.Multi-Relation Association Rules, MRAR          | Ассоциация                 | Обучение без учителя         |
| 25.High-order pattern discovery                    | Ассоциация                 | Обучение без учителя         |
| 26.SARSA                                           | Машинное обучение          | Обучение с подкреплением     |
| 27.DQN                                             | Машинное обучение          | Обучение с подкреплением     |
| 28.A3C                                             | Машинное обучение          | Обучение с подкреплением     |
| 29.Q-Learning                                      | Машинное обучение          | Обучение с подкреплением     |
| 30.Генетический алгоритм                           | Машинное обучение          | Обучение с подкреплением     |
| 31.Любой пример перебора алгоритмов                | Стэккинг                   | Ансамбли                     |
| 32.Random Forest                                   | Бэггинг                    | Ансамбли                     |
| 33.CatBoost                                        | Бустинг                    | Ансамбли                     |
| 34.LigntGBM                                        | Бустинг                    | Ансамбли                     |
| 35.XGBoost                                         | Бустинг                    | Ансамбли                     |
| 36.MLP                                             | Перцептрон                 | Нейронная сеть               |
| 37.CNN                                             | Сверточная сеть            | Нейронная сеть               |
| 38.DCNN                                            | Сверточная сеть            | Нейронная сеть               |
| 39.LSM                                             | Рекурентная сеть           | Нейронная сеть               |
| 40.RNN                                             | Рекурентная сеть           | Нейронная сеть               |
| 41.LSTM                                            | Рекурентная сеть           | Нейронная сеть               |
| 42.GRU                                             | Рекурентная сеть           | Нейронная сеть               |
| 43.GAN                                             | Генеративно-состязательная | Нейронная сеть               |
| 44.SEC2SEC                                         | Автокодировщик             | Нейронная сеть               |
| 45.Автокодировщик                                  | Автокодировщик             | Нейронная сеть               |
| 46.Импульсная (спайковая) НС                       | Импульсная                 | Нейронная сеть               |





## Примеры реализации                                 <a name='algo-example'></a>

Примеры реализации алгоритмов см. по ссылке ниже
* https://gitlab.com/dm_2008/ai-notes

## Тестовые наборы данных                             <a name='algo-data-sets'></a>

* Цветы (Ирисы)
    + [Описание работы с набором в sklearn](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_iris.html)
```
from sklearn.datasets import load_iris
ds = load_iris()

#X = ds.value.data
#y = ds.value.target
```

* Новости
    + [Описание набора данных](https://www.kaggle.com/crawford/20-newsgroups
    + [Описание работы с набором в sklearn](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.fetch_20newsgroups.html#sklearn.datasets.fetch_20newsgroups)

```
# Загрузка обучающей выборки по ограниченному набору категорий
from sklearn.datasets import fetch_20newsgroups

categories = ['sci.space','comp.graphics']
ds = fetch_20newsgroups(subset='train', categories=categories)

#X = ds.data
#y = ds.target
```

# Git Приемы работы                                   <a name='git'></a>
[К Оглавлению](#toc)

## Git-flow - краткое описание явления                <a name='git-flow'>

* см. статью [Модель ветвления GIT] (https://habr.com/ru/post/106912/)

Git-Flow - модель ведения проекта в репозитории. Предпологает одну основную
ветку `master`, в которой всегда доступна актуальная и рабочая версия проекта.
Остальные ветки ведутся паралельно и по мере завершения вливаются в метку
`master`. Если по чеснаку, то состав этих "остальных" веток у всех разный. Но
есть каноничный вариант, где выделяют следующие ветки:
* master - Всегда рабочая версия проекта
* dev - Следующая версия, которая по завршении будет влита в ветку release 
* feature-* - отдельные ветки для разрабов, которые вливаются в dev
* hotfix-* - экстренные изменения, вливаются и в dev и в master
* release-* - вся работа по выпуску версии, когда все готово, вот вот и... в ветку master


Илюстрация процесса "GitFlow" картинка из статьи "A successful Git branching model":

<img src="https://nvie.com/img/git-model@2x.png" alt="git flow" width="400"/>

Более подробно про GitFlow и критику на эту методику можно почитать:
* [GitLab Flow](https://habr.com/ru/company/softmart/blog/316686/)
* [Модель ветвления GIT](https://habr.com/ru/post/106912/)

## Основные команды GIT                               <a name='git-example' >

* Состояние, покажет текущую ветку и наличие незафиксированных изменений
```
git status
```


* Добавление новых файлов
```
git add -A
```


* Фиксация изменений (commit)
```
git commit -m "я все зделал, ну почти все"
```

* Присвоение тэга для ветки
```
git tag  -a 1.22
```


* Публикация изменений (push)
```
git push
```


* Добавление изменений в текущую ветку из ветки master
```
git merge master
```


* Переключение на ветку "feature-abc"
```
git checkout feature-abc
```


* Создание новой ветки "feature-abc"
```
git checkout -b feature-abc
```

* Публикация новой ветки на сервере
```
git push origin feature-abc
```

* Удаление ветки
```
git branch -d feature-abc
```

* Удаление ветки если ветка не удаляется
```
git branch -D feature-abc
```


* Список веток
```
git branch
```


* Вызов утилиты слияния (merge) при конфликте
```
git mergetool
```

* Выбор версии файла при слиянии (vimdiff3)
(см: справку в vim, см. [статью](https://medium.com/prodopsio/solving-git-merge-conflicts-with-vim-c8a8617e3633) )
```
:diffg RE  - использовать версию сервера
:diffg BA  - использовать общего предка
:diffg LO  - испльзовать локальную версию
```


* Навигация по конфликтам (vimdiff3)
```
[c  - Предыдущий конфликт
]c  - Следующий конфликт
```

## Настройка GIT tools                                <a name='git-setup' >
* Настройка утилиты merge на vimdiff
```
git config merge.tool vimdiff
git config merge.conflictstyle diff3
git config mergetool.prompt false
```

## CI-CD - Что это и зачем                            <a name='git-ci-cd'>

CI-CD - Принцип, по которому предполагается, что изменения в репо сразу
попадают на прод. Автоматически. Кроме того автоматические тесты, отчеты и все
такое.

* Ci - Continius Integration, Непрерывная интеграция
* Cd - Continius Delivery, Непрерывная доставка кода

Илюстрация из статьи "CI/CD: принципы, внедрение, инструменты" на хабре:
<img src="https://miro.medium.com/max/1400/0*7Ug0sGytwhy94O3Z.png" alt="ci-cd" width="600"/>

Ссылки:
* [Принципы внедрения CI-CD](https://medium.com/southbridge/ci-cd-принципы-внедрение-инструменты-f0626b9994c8)
* [Процесс разработки на примере django проекта](https://www.youtube.com/watch?v=vDshhQnXSfM)



## CI-CD Примеры                                      <a name='cicd-example'></a>

Каждой задаче можно присвоить тэг `stage`, и задать порядок выполнения этих этапов.
Задать можно еще много чего, например ручной запуск этапа `when: manual`.

Для отладки yaml скрипта в репо доступен валидатор по адресу
`<prodject>/-ci/lint`
[пример](https://gitlab.com/dm_2008/dm-system-up/-/ci/lint)

* .gitlab-ci.yaml:
```
stages:
- ontest
- test
- onprod

job 1:
    stage: test
    script:
    - echo "Job 2"

job 2:
    stage: ontest
    script:
    - echo "Job 1 - Deploy on Test"

job 3:
    stage: onprod
    when: manual
    script:
    - echo "Job 3 - !!! Deploy on Prod..."
```

* Ссылки на внешние примеры:

| Назначение скрипта                                      | Ссылка                                                  |
| :--                                                     | :--                                                     |
| Тестовый проект с настроенным CD                        | [dm_system_up](https://gitlab.com/dm_2008/dm-system-up) |
| **Примеры конфигурационных файлов CI-CD**               |                                                         |
| Несколько примеров собранных в одном файле              | [All in One File](./yml-example/allinone.gitlab-ci.yml) |
| Пример задания порядка при выполнении задач             | [Jobs order](./yml-example/jobsorder.gitlab-ci.yml)     |
| Доступ к удаленному серверу через SSH и пароль          | [SSH by Key](./yml-example/key.gitlab-ci.yml)           |
| Доступ к удаленному серверу через SSH и ключ шифрования | [SSH by Password](./yml-example/sshpass.gitlab-ci.yml)  |
| Пример группировки задач в один пакет                   | [Linked Jobs](./yml-example/linkedjobs.gitlab-ci.yml)   |

* Некоторые служебные слова:

| Термин              | Назначение                                                                                      |
| :--                 | :--                                                                                             |
| image               | Docker образ, может быть задан как для сценария целиком, так и для отдельной задачи             |
| script              | Начало секции с командами, bash, ruby и т.д в зависимости от выбранного образа                  |
| stages              | Перечень этапов и порядок их запуска                                                            |
| stage               | Назначение этапа для задачи                                                                     |
| include             | Ссылка на внешний файл сценария, итоговый файл сценария будет как результат склейки этих файлов |
| - local             | Для секции `include`, задает поиск файла в локальном репо                                       |
| allow_failure: true | Не останавливает конвеер при подении задачи с таким заданным свойством                          |
| when: manual        | Такая задача запускается только в ручную из Web-интерфейса                                      |
| - echo              | Пример bash команды `echo` в секции script                                                      |


## Скрипты развертывания тестовых сред                <a name='env-setup'></a>
[К Оглавлению](#toc)

| Назначение                                    | Ссылка                                                 |
| :--                                           | :--                                                    |
| Установка php 7.2 на ubuntu 18.4              | [php 7.2](./env-setup/ubuntu-php7.sh)                  |
| Установка Postgres 10.12 на ubuntu 18.4       | [Postgres 10.12](./env-setup/ubuntu-postgres10.sh)     |
| Создание Базы данных в установленном Posgtres | [create database](./env-setup/ubuntu-postgres10-db.sh) |

# Контейнеризация данных, стриминг                     <a name='streaming' ></a>
[К Оглавлению](#toc)

Внезапно оказалось, что streaming (Hadoop Streaming)  - это запуск
распределенных вычислительных задач на других языка.  Родным языком для запуска
таких задач является Java.  
Взаимодействие происходит черед стандартный поток ввода вывода - stdin\stdout.

## Пример реализации                                   <a name='streaming-example'>
Пример реализации процедуры подсчета слов в файле "wordcount" на Python

* countMap.py
```
#!/usr/bin/env python
import sys
for line in sys.stdin:
    for token in line.strip().split(" "):
        if token:
            print ('%s\t1' % token)
```

* countReduce.py
```
#!/usr/bin/env python
import sys
(lastKey, sum) = (None, 0)
for line in sys.stdin:
    (key, value) = line.strip().split('\t')
    if lastKey and lastKey != key:
        print ('%s\t%s' % (lastKey, sum))
        (lastKey, sum) = (key, int(value))
    else:
        (lastKey, sum) = (key, sum + int(value))
if lastKey:
    print ('%s\t%s' % (key, sum))
```


* countReduce.py (вариант с использованием словаря)
```
import sys

d = {}
for line in sys.stdin:
    try:
        key, value = line.strip().split("\t", maxsplit=1)
        d[key] = d.get(key, 0) + int(value)
    except ValueError:
        print(f"Invalid value: {value}. Must be int.")

for key, value in d.items():
    print(f"{key}\t{value}")
```

 * Отладочный запуск
 ```
>cat test.txt | ./countMap.py | sort | ./countReduce.py
 ```

 * Запуск задачи с помощью Hadoop Streaming
```
>hadoop jar $HADOOP_HOME/hadoop/hadoop-streaming.jar 
    -D mapred.job.name="Word count Job via Streaming" \
    -files countMap.py, countReduce.py \
    -input text.txt \
    -output /tmp/wordCount/ \
    -mapper countMap.py \
    -combiner countReduce.py \
    -reducer countReduce.py
```
 


# Postgres                                             <a name='postgres'></a>
[К Оглавлению](#toc)

Сервер баз данных.

См. далее:
* Установка
* Переключение на другой каталог с данными

## Postgres - Установка                                <a name='postgres-install'></a>

Примерные действия по установке Postgres v 12.

* Обновление системы
```
sudo apt update
sudo apt -y install vim bash-completion wget
sudo apt -y upgrade
```

* Установка сервера Postgres 12
```
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" |sudo tee  /etc/apt/sources.list.d/pgdg.list
sudo apt update
sudo apt -y install postgresql-12 postgresql-client-12
```


* Проверка состояния сервера
```
systemctl status postgresql.service 
systemctl status postgresql@12-main.service
systemctl is-enabled postgresql
```

* Остановка сервера
```
systemctl stop postgresql@12-main.service
```

* Запуск сервера
```
systemctl start postgresql@12-main.service
```

* Подключение к серверу
```
sudo -u postgres psql -p 5433
```

* Информация о подключении
```
sudo -u postgres psql -p 5433 -c "\conninfo"
```

* Список баз
```
sudo -u postgres psql -p 5433 -l
```

* Список таблиц
```
sudo -u postgres psql -p 5433 -c "\l"
```

* Файл настроек
    > /etc/postgresql/12/main/postgresql.conf

* Настройка каталога с данными
    > см. файл /etc/postgresql/12/main/postgresql.conf
    > Параметр: data_directory
* Команда в бд
```
sudo -u postgres psql -c "show data_directory;"
```

## Упражнение - Подключение каталога с данными         <a name='postgres-catalog'></a>

Порядок действий:
* Процедура подмены каталога
* Остановить сервер
* Изменить каталог хранения `data_directory`
* Запустить сервер
* Посмотреть статус
* Посмотреть локали системы
* Установить локаль (по необходимости)
* Остановить сервер
* Запустить сервер
* Подключится к базе

```
systemctl stop postgresql@12-main.service
sudo vim /etc/postgresql/12/main/postgresql.conf
systemctl start postgresql@12-main.service
systemctl status postgresql@12-main.service
sudo -u postgres psql -p 5433 -c "show data_directory;"

locale -a
sudo locale-gen ru_RU.UTF-8

systemctl stop postgresql@12-main.service
systemctl start postgresql@12-main.service
systemctl status postgresql@12-main.service

sudo -u postgres psql -p 5433 -l
sudo -u postgres psql -p 5433 -c "show data_directory;"
sudo -u postgres psql -p 5433 -c "\dt *"
```


# CRISP-DM                                            <a name='crisp'></a>
[К Оглавлению](#toc)

Методика, описывает процесс разработки ML решения.

Т.е. как надо правильно разрабатывать ML решения. 

Содержит задачи, разделенные на этапы. Для каждой задачи определен искомый результат.

~~Создана для манагеров, которые нихера не могут понять, что-же делают эти
инженеры, а руководить так хочется. Короче вот задачи, иди выполняй, давай результат.~~

Вобщем еще чуть чуть... и проектных менеджеров можно заменить одним алогритмом
натянутым на другой.

Это была попытка пошутить, если вдруг кто не понял ;)


Ссылки:
* kdnuggets.org
* https://habr.com/ru/company/lanit/blog/328858/
* https://www.the-modeling-agency.com/crisp-dm.pdf
* ftp://ftp.software.ibm.com/software/analytics/spss/documentation/modeler/14.2/en/CRISP_DM.pdf

Свойства:
* Внимание на бизнес-целях компании
* Детальное документирование каждого шага

Этапы:
* Бизнес анализ, понимание и осознание
* Анализ данных, понимание и осознание
* Подготовка данных
* Моделирование
* Оценка результата
* Внедрение

Ссылка на картинку из статьи на хабре: "CRISP-DM: проверенная методология для Data Scientist-ов"

<img src="https://hsto.org/getpro/habr/post_images/77b/e0f/8b5/77be0f8b5b1c7d9ad368d347a087b658.jpg" alt="phases" width="400"/>

* Сравнение этапов в CRISP-DM и SEMMA

| CRISP-DM              | SEMMA           | По русски           |
| :--                   | :--             | :--                 |
| Buisnes understanding |                 | Что нужно заказчику |
| Data understanding    | Sample, Explore | Как организованы данных, и что в них есть |
| Data Preparation      | Modify          | Очистка данных      |
| Modeling              | Model           | Создание и отбор моделей |
| Evalution             | Assess          |
| Deployment            |                 |

Вроде ничего общего? Или мне что-то начинает казаться...

<img src="https://img.uslugio.com/img/c2/a0/c2a0d275bf8f85dbe1055976b5460fa0.jpg" alt="wow" width="200"/>


## Бизнес анализ, понимание и осознание
**(Buisnes Understanding)**

Основная задача этого этапа - понять, чего хочет заказчик. 

Здесь важно понимать и отличать требования заказчика от его потребностей, т.е.
что заказчик декларирует, и что-же ему на самом деле нужно?

* Организационная структура
    - Кто? (участники со стороны заказчика)
    - Кто? (принимает решения)
    - Кто? (основной пользователь)
    - Кто? (выделяет деньги)
    - Сколько? (денег выделяют)
    - Где? (контакты основных участников проекта)
* Бизнес цель проекта?
* Наличие реализованных решений (оценка предыдущего опыта)
    - какие решения существуют?
    - чем не устраивают?
* Текущая ситуация
    - Сколько? (оценка ресурсов для проекта)
    - Сколько? (доступного железа)
    - Сколько? (оборудования не хватает и надо закупать)
    - Сколько? (дополнительно требуется собрать внешних данных)
    - Где? (хранятся данные)
    - Как? (производится доступ к данным)
* Риски
    - Не уложится в сроки
    - Финансовые риски
    - Достаточное количество данных
    - Наличие закономерностей в данных
    - ROI (Окупаемость инвестиций), оценка финансового эффекта от внедрения
* Цели Data Mining
    - Какая метрика для оценки качества модели
        + Accuracy
        + RMSE
        + AUC
        + Precision
        + Recall
        + F-mesure
        + R-sqare
        + Lift
        + LogLoss
    - Критерий успешности модели
        + Min: AUC - 0.65
        + Optimum: AUC- 0.75
    - Как оценивать, если метрики не устраиватют заказчика?
* План
    - План с оценкой по времени всех оставшихся 5-ти фаз, а лучше сразу всех 6-ти.
    - Сколько? (времени займет реализация)
    - Как? (из каких шагов, вех и этапов будет состоять проект)


## Анализ данных, понимание и осознание
**(Data Understanding)**

Смотрим на данные, ищем сильные и слабые стороны, достаточность данных, идеи и т.п.

* Сбор данных
    - собственные данные (уже есть)
    - сторонние данные (уже нашли, но надо собрать)
    - потенциальные данные (где-то есть, но мы не знаем)
* Описание данных
    - описание данных (структура)
    - сэмплирование данных (если данных слишком много)
    - сбор ключевых статистик
* Исследование данных
    - графики
    - гипотезы
    - замеченные закономерности
* Качество данных
    - пропущенные данные
    - опечатки
    - нарушение целостности и связанности
    - множественные кодировки справочников (m, male, Муж)
    - кодировки символов

## Подготовка данных
**(Data Preparation)**

Подготовка обучающей выборки для дальнейшего обучения моделей.

По оценкам разных специалистов может занимать более 80% всего времени проекта.

* Отбор данных
    - атрибут полезен для решаемой задачи?
    - атрибут качественный?
    - включать корелирующий атрибут?
    - ограничения на использование атрибутов?
* Очистка данных
    - Как? (что делаем с пропущенными значениями)
    - Как? (что делаем с ошибками)
    - Как? (что делаем с различающимися кодировками)
* Генерация данных
    - агрегация атрибутов
    - создание "кйсов"
    - конвертация типов
    - нормализация атрибутов
    - заполнение пропущенных значений
* Интеграция данных
    - Где? (выбор хранилища)
    - Как? (определение структуры для обучащюей выборки)
    - Объединение разрозненных наборов 
        + вертикальное (по строкам, append, insert)
        + горизонтальное (по столбцам, merge, join)
    - И еще раз агрегация
* Форматирование данных
    - по необходимости форматирование
    - по необхоодимости сортировка
    - по необходимости индексирование

## Моделирование
**(Modeling)**

* Выбор алгоритмов
    - данных достаточно?
    - что делать с пропусками, модель обработает, или в ручную?
    - что делать с типами, нужна ли конвертация?
* Планирование тестирования
    - или деление на 3 части
    - или перекрестная проверка (cross-validation)
    - grid search
    - random search
* Обучение моделей
    - найдены ли закономерности?
    - какова скорость обучения?
    - качество данных?
* Оценка результатов
    - технический анализ каждой модели (количественная оценка по метрикам: ROC, Gian, Lift и т.п.)
    - модель готова к внедрению?
    - критерии качества достигнуты?
    - бизнес цели достигнуты?
    - результат моделирования понятен?
    - результат моделироания логичен?
    - опробованы все доступные модели?
    - техническая возможность для внедрения есть?

## Оценка результата
**(Evolution)**

* Оценка результатов моделирования
    - результаты в бизнес терминах (ROI и Рубли, Доллары)
    - качественная оценка, на сколько хорошо модель решает поставленную бизнес задачу
    - найдена ли новая ценная информация
* Разбор полетов
    - можно ли было сделать лучше\эффективнее
    - допущенные ошибки
    - были ли нерабочие гипотезы?
    - были ли неожиданности?
* Принятие решения
    - у модели есть потенциал?
    - модель устраивает заказчика
    - отбор лучшей модели из нескольких
    - улучшаем дальше или внедряем или паралельно?

## Внедрение
**(Deployment)**

* Планирование
    - что и в каком виде внедряем
    - технический план внедрения
        + пароли
        + что
        + куда
        + когда
        + как
    - как с моделью работают пользователи
    - как проводится мониторинг 
* Настройка мониторинга
    - что отслеживаем
    - какие свойства модели отслеживаем
        + PSI
    - как понимаем что модель устарела
    - что делать если модель устарела
* Отчет по результатам моделирования
    - отчет по каждому шагу
    - рекомендации по дальнейшему развитию модели


## CRISP: Общие термины и определения                 <a name='crisp-terms'>

| Термин                 | Пояснение                        |
| :--                    | :--                              |
| предиктивная модель    | На выходе модель выдает прогноз. |
| прескриптиивная модель | На выходе модель выадет рекомендации. |
| CRISP-DM               | Cross Industry Standart Process for Data Maining. Стандарт, которые описывает общие подходы к аналитике данных. |
| y = f(x)               | Функциональная зависимость, но как это объяснить бизнесу? Это уж как повезет. |
| модель                 | Если `y` искомая величина, то `f(x)` ~~это и есть наша серебрянная пуля, которая решит все проблемы.~~ Нет, все-же это просто программа, которая дает некий результат. Если повезет, то этот результат может иметь прогностическую ценность. |
| Обучающая выборка      | Таблица, содержащая пары x-y. Столбцы этой таблицы - атрибуты. |
| Предиктор              | Тот же столбец из обущающей выбрки, но только обладающий большой предсказательной значимостью. |
| Скоринг                | По нашему - это оценка. Когда истенный y не известен, значение полученное с помощью модели называют score. |
| Кейс                   | В теории - это кортеж. Или еще проще - строка, запись в обучающей выборке. |

## CRISP: Упражнение                                   <a name='crisp-task'></a>
[К Оглавлению](#toc)

Перечисли основные этапы производственного процесса, построенного по методологии CRISP.

* Блокнот с упражнением [здесь](./examples/meta/crisp-main-stages.ipynb)
* Блокнот с решением [здесь](./examples/meta/crisp-main-stages-task.ipynb)

# SEMMA                                               <a name='semma'></a>
[К Оглавлению](#toc)

Методология ведения процесса разработки аналитических моделей.

Получила свое название по первым буквам этапов разработки модели.

В основе идея организация функций по целевому назначению. Применяется в
инструменте SAS Interprice Mainer.

Данную методику часто критикуют за то, что она сфокусированна на технической
стороне, оставля потребности бизнеса за скобками.

По мне, так в два последних шага, хоть весь бизнес целиком можно запихать, и
еще место останется.

Этапы:

* Sample - Обучающая выборка должна быть репрезентативной, но важно не перестаратся с размерами, для более легкого применения в процессе выбора моделей.
* Explore - Поиск шаблонов и закономерностей.
* Modify - Создание новых атрибутов, удаление незначимых.
* Model - Выбор лучшей модели для текущей ситуации.
* Assess - Определение применимости модели, тестирование на другой выборке.

Ссылки:
* http://jesshampton.com/2011/02/16/semma-and-crisp-dm-data-mining-methodologies/

## SEMMA: Общие термины и определения                 <a name='semma-terms'></a>

| Термин  | Определение                     |
| :--     | :--                             |
| SEMMA   | Акроним, расшифровка ниже в этой таблице. Описывает процесс разработки математической модели. |
| Sample  | Выбор. Этап отбора данных.      |
| Explore | Обзор. Этап иследования данных. |
| Modify  | Изменение. Этап проектирования новых атрибутов и обработки данных. |
| Model   | Моделирование. Моделирование и оценка резульатов модели, выбор лучшей модели из разрабатываемых. |
| Assess  | Оценка. Определение применимости и значимости модели, тестирование на других выборках данных.    |

## SEMMA: Упражнение                                   <a name='semma-task'></a>
[К Оглавлению](#toc)

Перечисли основные этапы производственного процесса, построенного по методологии SEMMA.

* Блокнот с упражнением [здесь](./examples/meta/semma-main-stages.ipynb)
* Блокнот с решением [здесь](./examples/meta/semma-main-stages-task.ipynb)


# Agile                                               <a name='agile'></a>
[К Оглавлению](#toc)

"Аджайл" - одна из модных методик организации производтственного процесса в IT команде.

Каждая итерация разработки состоит из 3х этапов:
* Старт
* Спринт (Забег)
* Внедрение

Где этапы старт и внедрения сами по себе атомарные единицы, а вот спринт сам в свою очередь состоит из этапов:
* Планирование
* Разработка
* Тестирование
* Демонстрация

Тут все-бы ничего, но почему-то есть мнение, что после демонстрации
автоматически следует внедрение. Хотя из из самого манифеста это как раз и не
следует. А с учетом того, что в этой методике бизнес не формулирует четкие
требования, т.е. "не подписывает контракт", то и предмета внедрения может не
оказатся.

    Опять попытка натянуть западную сову - технологию
    на русский глобус.

    А еще похоже, там за лужей, даже своих классиков не читают.  Фредерик Брукс
    в своем сборнике "Мифический человеко-месяц" хорошо и качественно описал
    процесс разработки, но нет, мы пойдем другим путем...

~~Особенности~~ Заблуждения:
* Детальное задание не обязательно
* Разработка под контролем
* Низкая себестоимость
* Рисков нет

Ссылки:
* [Манифест](http://agilemanifesto.org/iso/ru/principles.html)
* [Видео на 5 мин](https://youtu.be/TPrj-AMJ4Ds)
* [Книга](http://scrum.org.ua/wp-content/uploads/2008/12/scrum_xp-from-the-trenches-rus-final.pdf)
* [Мифический человеко-месяц](https://e-libra.ru/read/178550-mificheskiy-cheloveko-mesyac-ili-kak-sozdayutsya-programmnye-sistemy.html)



## Agile: Общие термины и определения                 <a name='agile-terms'></a>

| Термин                 | Пояснение                                            |
| :--                    | :--                                                  |
| Спринт                 | Понятный и кортокий производственный план на ближайшую неделю или две. Принято считать, что оптимальный забег длится от 1 до 4х недель. |
| StendUp                | Совещание 5ти минутка, но не более 15-ти минут. Но по факту... |
| Скрам                  | Метод ведения работы IT команды, в которм декларируется однозадачность, и вешается доска на стену. |
| Скрам                  | Вообще-то это один из способов игры в регби. А у нас как известно, в регби играют мало. |
| BackLog                | Пожелания заказчика к текущему спринту.              |
| МЖП                    | Минимально жизнеспособный продукт.                   |
| Story-Point            | Человеко-день. Могу предположить, что другое название дали по соображениям лицензионной гигиены. |
| Burn-Down              | Чаще всего это график. Отражает отклонение производственного процесса от намеченного плана. |


# Вспомогательные инструменты                         <a name='assets'></a>
[К Оглавлению](#toc)

## Vagrant - командная оболочка для virtual box       <a name='assets-vagrant'></a>
Оболочка для управления виртуальными машинами через консльные команды.
Позваляет автоматизировать даже стягивание образа из сети, и последующий
запуск.  Вобщем вполне себе альтернатива `docker`, только с полной
виртуализацией и без луковичной структуры.

Особенности:
* Все натройки гостевой системы задаются в файле `Vagrantfile`.
* По умолчанию все команды относятся к ближайшему `Vagrantfile`.

Ресурсы:
* Документация: https://www.vagrantup.com/docs/index.html
* Образы виртуальных машин: https://app.vagrantup.com/boxes/search

### Vagrant - Основные команды                          <a name='assets-vagrantcmd'></a>
В таком варианте все команды выполняются в директории, где лежит
соответствующий файл конфигурации `Vagrantfile`.

| Команда         | Описание                                    |
| :---            | :---                                        |
| vagrant up      | Запуск виртуальной машины                   |
| vagrant ssh     | Подключение к консоли виртуальной машины    |
| vagrant halt    | Остановка виртуальной машины                |
| vagrant suspend | Приостановка машины (сон)                   |
| vagrant resume  | Восстановление машины из состояния сна      |
| vagrant destroy | Удаление виртуальной машины, насовсем       |
| vagrant init    | Создает **подробно комментированный** Vagrantfile в текущем каталоге |





### Vagrant - Подключение по SSH                          <a name='assets-vagrantssh'></a>
Достучатся до виртуальной машины можно двумя способами:
* Используя командную оболочку Vagrant - `vagrant ssh`
* Используя системные средства - `ssh`

Пример:
```
ssh vagrant@127.0.0.1 -p 2222 -i .vagrant/machines/default/virtualbox/private_key
```

В Vagrant приняты следующие настройки, которые впрочем можно изменить:
* Основной пользователь: vagrant
* Пароль пользователя: vagrant
* 22й порт для доступа по ssh перебразывается на 2222й порт базовой машины
* Секретный ключ находится в папке виртуальной машины по адресу `./.vagrant/machines/default/virtualbox/private_key`

### Vagrant - файл настроек `Vagrantfile`              <a name='assets-vagrantfile'></a>
Все команды, такие как `vagrant up` выполняются в папке, где лежит файл
настроек `Vagrantfile`.  Файл Vagrant может быть создан командрой `vagrant init
<image name>`. А дальше открывается широкое поле по автоматической настрокие
виртуальной машины.

* Пример минимальной конфигурации
```
Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
end
```

* Пример конфигурации с заданным именем виртуальной машины `mainframe7000`
```
Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"
  config.vm.provider :virtualbox do |vb|
      vb.name = "mainframe7000"
  end
end
```


### Vagrant - Создание виртуальной машины              <a name='assets-vagrantcreateexample'></a>

* Создание виртуальной машины CentOS 7
* https://app.vagrantup.com/centos/boxes/7
```
mkdir my_box
cd my_box
vagrant init centos/7
vagrant up
```

### Vagrant - Примеры конфигурации <a name='assets-vagrantconfig'></a>

#### Пример конфигурациооного файла (Ubuntu 14.04)
Виртуальная машина [Ubuntu 14.04 LTS](https://app.vagrantup.com/ubuntu/boxes/trusty64) доступна в репозитории.

Данная конфигурация пригодна для запуска скрипта [Простейший web-сервер](#http-server).
Для запуска требуется:
* Создать папку проекта, например `pws`
* Создать файл `./pws/Vagrantfile` как указано ниже:
* Создать папку `./pws/simple-web`
* Создать файл `./pws/simple-web/index.html` как указано [здесь](#http-server)
* Создать файл `./pws/simple-web/web-server.pyl` как указано [здесь](#http-server)
* Запустить виртуальную машину, находясь в папке `./pws` выполнить команду `vagrant up`
* Подключится к консоли, находясь в папке `./pws` выполнить команду `vagrant ssh`
* Находясь в подключенной консоли выполнить команду `cd /vagrant && python3 web-server.py`
* Web-server доступен в броузере по адресу http://localhost:8887

Файл `Vagrantfile`:
```
Vagrant.configure("2") do |config|
  config.vm.provider :virtualbox do |vb|
      vb.name = "simple-web-server"
  end

  config.vm.box = "ubuntu/trusty64"
  config.vm.network "private_network", ip: "192.168.33.10"
  config.vm.network :forwarded_port, guest: 8080, host: 8887, auto_correct: true
  config.vm.synced_folder "./simple-web", "/vagrant"
end
```



#### Пример конфигурациооного файла (Ubuntu 18.04 Lts)
Виртуальная машина [Ubuntu 18.04 LTS](https://app.vagrantup.com/generic/boxes/ubuntu1804) доступна в репозитории.

Данная конфигурация пригодна для запуска скрипта [Простейший web-сервер](#http-server).
Для запуска требуется:
* Создать папку проекта, например `pws`
* Создать файл `./pws/Vagrantfile` как указано ниже:
* Создать папку `./pws/simple-web`
* Создать файл `./pws/simple-web/index.html` как указано [здесь](#http-server)
* Создать файл `./pws/simple-web/web-server.pyl` как указано [здесь](#http-server)
* Запустить виртуальную машину, находясь в папке `./pws` выполнить команду `vagrant up`
* Подключится к консоли, находясь в папке `./pws` выполнить команду `vagrant ssh`
* Находясь в подключенной консоли выполнить команду `cd /vagrant && python3 web-server.py`
* Web-server доступен в броузере по адресу http://localhost:8887

Файл `Vagrantfile`:
```
Vagrant.configure("2") do |config|
  config.vm.provider :virtualbox do |vb|
      vb.name = "simple-web-server"
  end

  config.vm.box = "generic/ubuntu1804"
  config.vm.network "private_network", ip: "192.168.33.10"
  config.vm.network :forwarded_port, guest: 8080, host: 8887, auto_correct: true
  config.vm.synced_folder "./simple-web", "/vagrant"
end
```

Создание файла `Vagrantfile`:
```
vagrant init generic/ubuntu1804
vagrant up
```


#### Пример конфигурациооного файла (полигон Hadoop + Spark + Jupyter)
Виртуальная машина `sparkpy/sparkbox_test_1` доступна в репозитории.
https://app.vagrantup.com/sparkpy/boxes/sparkbox_test_1

Для создания виртуальной машины требуется:
* Создать удобную папку
* В папке создать Vagrantfile
* Заполнить файл как указано ниже
* В этой папке выполнить команду `vagrant up`
* Дождатся завершения работы скрипта

Содержимое файла `Vagrantfile`:
```
Vagrant.configure("2") do |config|
    config.vm.box = "sparkpy/sparkbox_test_1"
    config.vm.hostname = "sparkbox"
    config.ssh.insert_key = false

    # Менеджер ресурсов Hadoop 
    config.vm.network :forwarded_port, guest: 8088, host: 8088, auto_correct: true

    # Узел имен Hadoop 
    config.vm.network :forwarded_port, guest: 50070, host: 50070, auto_correct: true

    # Узел данных Hadoop 
    config.vm.network :forwarded_port, guest: 50075, host: 50075, auto_correct: true

    # Блокноты Jupyter Ipython (yarn and standalone)
    config.vm.network :forwarded_port, guest: 8888, host: 8888, auto_correct: true

    # Spark UI (Автономный)
    config.vm.network :forwarded_port, guest: 4040, host: 4040, auto_correct: true
    config.vm.provider "virtualbox" do |v|
        v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
        v.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
        v.customize ["modifyvm", :id, "--nictype1", "virtio"]
        v.customize ["modifyvm", :id, "--cpuexecutioncap", "60"]
        v.name = "sparkbox_test"
        v.memory = "6144"
        v.cpus = "2"
    end
end
```

#### Выполнение команды при старте виртуальной машины (provision)
Для запуска скриптов есть два способа, через триггер, и через provisioner `shell`.
Почитать можно здесь: https://www.vagrantup.com/docs/provisioning/shell.html

* Файл `Vagrantfile`:
```
$startup = <<SCRIPT
#####!/usr/bin/env bash
echo "Start Up process..."
bash -c "pwd"
bash ./all-start.sh
SCRIPT

Vagrant.configure("2") do |config|
    ...
    config.vm.provision :shell, inline: $startup, run: 'always'
    ...
end
```

#### Выполнение команды при старте и остановке виртуальной машины (тригер)
Особенность тригера в том, что обращение идет к бинарному файлу. Необходимо
писать вызов командного файла с `bash`. 

Скрипты находятся в папке пользователя vagrant.

Документация доступна по адресу:
https://www.vagrantup.com/docs/triggers/

* Файл `Vagrantfile`:
```
Vagrant.configure("2") do |config|

    ...

    # Запуск при старте
    config.trigger.after :up do |trigger|
        trigger.info = "Start up process..."
        trigger.run_remote = {inline: "bash /home/vagrant/all-start.sh"}
        #trigger.run_remote = {inline: "bash ./all-start.sh"}
    end

    # Запуск при остановке
    config.trigger.before :halt do |trigger|
        trigger.warn = "Start Shutdown process..."
        trigger.run_remote = {inline: "bash /home/vagrant/stop-hadoop.sh"}
        #trigger.run_remote = {inline: "bash ./stop-hadoop.sh"}
    end
end

```




## CRON - Планировщик заданий в linux <a name='assets-cron'></a>
### Основные команды

Удалить расписание для пользователя
> crontab -r

Редактировать рассписание для пользователя
> crontab -e

Вывести файл рассписания на экран
> crontab -l

Запуск планировщика, если не запущен
> service cron start

Состояние планировщика
> service cron status
```
    ● cron.service - Regular background program processing daemon
       Loaded: loaded (/lib/systemd/system/cron.service; enabled; vendor preset: ena
       Active: active (running) since Вт 2020-06-16 08:48:21 MSK; 3 days ago
         Docs: man:cron(8)
     Main PID: 1209 (cron)
        Tasks: 1
       Memory: 1.0M
          CPU: 6.286s
       CGroup: /system.slice/cron.service
               └─1209 /usr/sbin/cron -f
```

* Лог работы планировщика
grep cron /var/log/syslog

* Ошибки планировщика
tail -f /var/log/syslog | grep --line-buffered cron

* Постоянный вывод изменений из любого файла
tail -f /some/dir/minute-at.log


                                      * * * 
### Пример файла рассписания
* Для редактирования использовать `crontab -e`
```
01 SHELL=/bin/bash
02 1 *   *   *   *    bash /some/dir/minute-cmd-run.sh
03 */1 *   *   *   *    bash /some/dir/minute-at-run.sh
04
```

* файл minute-cmd-run.sh
```
01 #!/usr/bin/bash
02 echo "cmd min $(date '+%A %W %T')" >> /some/dir/cron-cmd-min.log
```

* Отслеживание изменений в файле
```
cd /some/dir
touch cron-cmd-min.log
tail -f cron-cmd-min.log
```



### Последовательность действий для первого тестового запуска
* создать директорию /some/dir
> mkdir /some/dir
> cd /some/dir

* создать командный файл /some/dir/minute-at-run.sh
```
01 #!/usr/bin/bash
02 echo "cmd min $(date '+%A %W %T')" >> /some/dir/cron-at-min.log
```

* Создать рассписание, пустая строка в конце файла обязательна!
> crontab -e
```
1 *   *   *   *    bash /some/dir/minute-cmd-run.sh
*/1 *   *   *   *    bash /some/dir/minute-at-run.sh


```
* Проверить состояние сервиса
> service cron status

* Запстить по необходимости
> service cron start

* Запустить отслеживание файла лога
> tail -f /some/dir/cron-at-min.log

* Ждать 1 минуту, наблюдать результат

* Ошибки можно посмотреть тут
tail -f /var/log/syslog | grep --line-buffered cron



# Статистика                                          <a name='statistics'></a>
[К Оглавлению](#toc)

## Комбинаторика                                     <a name='stat-combinations'></a>
### Перестановка (permutation)
* Перестановка - число возможных вариантов, как можно расставить n объектов.
```
N = n!
```

### Выборка с возвращением (variation with reptition)
Выборка с возвращением (выборка), число различных возможных способов выбрать
набор размером `k` из `n` доступных элементов. Элементы могу повторятся.
```
N = n^k
```
Где:
* N - число вариантов выборки
* n - Общее число элементов, доступных для выбора (алфавит)
* k - число ячеек или мест, на которые необходимо выбрать доступные `n` элементов.

### Размещение (variations without repetiion)
* Число размещений, упорядоченный набор по `m` элементов, из `n` без повторений.
* Наборы: {Вася, Петя, Оля} и {Оля, Петя, Вася} считаются разными наборами.
* т.е. в данном случае, позиция является значимой.
см: https://www.mathelp.spb.ru/book2/tv3.htm
```
    V = n! / (n - m)!
```
Где:
* A - Число размещений
* n - Общее число элементов, доступных для выбора
* m - число ячеек или мест, на которые необходимо выбрать достумные `n` элементов.

### Сочетание (combinations)
* Число сочетаний, неупорядоченный набор по `m` элементов, из `n` без повторений.
* Наборы: {Вася, Петя, Оля} и {Оля, Петя, Вася} считаются одним и тем-же набором.
* т.е. в данном случае, позиция не является значимой.
```
    С = n! / m!(n - m)!
```
Где:
* С - Число сочетаний
* n - Общее число элементов, доступных для выбора
* m - число неповторяющихся элементв, которые необходимо выбрать.

## Задача 1. Выборка с возвращением                  <a name='stat-varwithrep'></a>
На сумке установлен 3х значный кодовый замок. На каждом барабане числа от 0 до 7. Какое
количество вариантов доступно для выбора в качестве кода?
```
0,1,2,3,4,5,6,7 - Всего 8 символов, n = 8
3 ячейки, k = 3

N = n^k = 8^3 = 512
```
Ответ: 512 вариантов

## Задача 2. Выборка с возвращением Установлен 3х-значный код, в каждой ячейке доступны буквы английского алфавита,
всег 26 букв. Какое количество вариантов доступно для выбора в качестве кода?
```
n = 26
3 ячейки, k = 3

N = n^k = 26^3 = 17576
```
Ответ: 17576 вариантов

## Задача 3. Выборка с возвращением
8и-ричная система исчисления, доступны цифры от 0 до 7. Сколько 3х значных и
четных чисел можно составить в этой системе?
```
1. Для первой позиции доступны цифри от 1 до 7, n1 = 7
2. Для второй позиции доступны цифры от 0 до 7, n2 = 8
3. Для третьей позиции доступны цифры 0,2,4,6,  n3 = 4
3 ячейки, k = 3, здесь смысла не имеет, алфавиты разные в каждой ячейке, т.е. k1=k2=k3=1

N = n1^k * n2^k * n3^k = n1 * n2 * n3 = 224
```
Ответ: 224 вариантов

## Задача 4. Число размещений                        <a name='stat-varwithoutrep'></a>
Есть 7 возможных кандидатов для забега и 4 стартовых места. Необходимо выбрать
4х участников и рассположить их на по стартовым местам.

Какое количество вариантов доступно.
```
V = 7! / (7 - 4)! = 7!/3! = 840
```
Ответ: 840 доступных вариантов.

## Задача 5. Число размещений
Есть 7 возможных кандидатов для забега и 4 стартовых места. Необходимо выбрать
4х участников и рассположить их на по стартовым местам. Но дополнительно необходимо поставить 1го участника в резерв.

Какое количество вариантов доступно.
```
4+1 = 5
V = 7! / (7 - 5)! = 7!/2! = 2520
```
Ответ: 2520 доступных вариантов.

## Задача 6. Число размещений
Повар Вася делает 3х уровневый торт. На каждом уровне он рассполагает цветочки,
на каждом уровне свой вид цветочков.  Офигенный повар вася не умеет сам делать
цветочки, поэтому он заказывает их в соседнем кондитерском киоске по 1.20 за штуку. Но в
киоске давно полный бордак, и они смогли найти только 5 разных видов цветочков.
Сколько уникальных вариантов оформления тортика доступно повару Васе?
```
V = 5! / (5-3)! = 5!/2! = 60
```
Ответ: 60 доступных вариантов.

## Задача 7. Число сочетаний
Повар Вася приехал в Париж, и зашел в самый известный ресторан `Rogya and Kopytiya`.
В меню доступно 8 различных блюд из макарон. но Вася понимает, что больше 3х ему не съесть.
Сколько различных сочетаний блюд доступно Василию?
```
V = 8! / 3!(8-3)! = 5!/(3!5!) = 56
```
Ответ: 56 доступных вариантов.

## Задача 8. Число сочетаний
Повар Вася приехал в Париж, и зашел в самый известный ресторан `Rogya and Kopytiya`.
В меню доступно 8 различных блюд из макарон. но Вася понимает, что больше 3х ему не съесть.
Но к Василию присоеденился Петро, и сказал, что он готов попробовать еще 2 блюда, и поделиться впечатлениями.
Таким образом, на двоих они готовы взять 5 различных блюд.
Сколько различных сочетаний блюд доступно Василию и Петру Ивановичу?
```
V = 8! / 5!(8-5)! = 8!/(5!3!) = 56
```
Ответ: И опять 56 доступных вариантов, зря потратились на Петю.

## Задача 9. Число сочетаний
Повар Вася приехал в Париж, и зашел в самый известный ресторан `Rogya and Kopytiya`.  
В меню доступно 8 различных блюд из макарон. И Вася принимает
героическое решение, попробовать их все 8, даже наплевав на то, что он все не
съест. Но зато попробует!
Сколько различных сочетаний блюд доступно Василию?
```
V = 8! / 8!(8-8)! = 8!/1 = 8
```
Ответ: И опять 56 доступных вариантов, зря потратились на Петю.



                                      * * * 

# Паралельная реальность                                 <a name='other-tools'></a>

Здесь собраны вспомогательные материалы по инструментам, которые не имеют прямого отношения к "Большим данным".
Однако...

## Framework Django                                                    <a name='django'></a>
Сборник примеров для Django с описанием

* [Базовые команды в Django](#django-cmd)
* [Параметр на странице](#django-params)
* [Авторизация в Django](#django-auth)
* [Отладка в Django](#django-debug)




### Базовые команды в Django                             <a name='django-cmd'></a>

* Создать новый проект
```
> django-admin startproject some_project
```
* Создать новое приложение
```
> cd some_project
> ./manage.py startapp some_app
> ./manage.py migrate
```
* Создание административной записи
```
> cd some_project
> ./manage.py createsuperuser
```
* Запуск отладочного web сервера
```
> cd some_project
> ./manage.py runserver
```
* Применить изменения моделей к базе данных
```
> cd some_project
> ./manage.py migrate
```

### Параметр на странице                                 <a name='django-params'></a>
Словарь:
* django.shortcuts, render
* django.urls, include
* render (request, '.html', context)
* {{ form.param_id }}

Задача:
В адресной строке принимать параметры, передавать их в представление.
Переданные параметры выводить на странице.

Действия:
* Определить ссылку с параметрами в urls.py
* Проверить проброс ссылок в приложении
* Определить представление с параметрами в views.py
* Подготовить шаблон с заданым контекстом в templates/web_param/some.html

Пример:

* Модификация файла web_param/urls.py
```
...
from . import views

...
urlpatterns = [
...
    path('context/<int:query_id>/<int:param_id>', views.param_context),
    path('direct/<int:query_id>/<int:param_id>', views.param_direct),
...
]

```

* Модификация файла webengine/urls.py
```
...
from django.urls import path, include
...
urlpatterns = [
    path('', include('web_param.urls')),
...
```

* Модификация файла web_param/views.py
(для контекстной передачи параметра)
```
...
from django.shortcuts import render
...
def param_context(request, query_id, param_id):
    form = {}

    form['query_id'] = query_id
    form['param_id'] = param_id

    context = {'form': form}
    return render (request, 'web_param/param.html', context)
...
```

* Модификация файла web_param/views.py
(для формирования страницы "на лету")
```
...
from django.http import HttpResponse
...
def param_direct(request, query_id, param_id):
    resp='<h1>Параметры по запросу</h1>'
    resp+='<ul>'
    resp+='<li>query_id: ' + str(query_id) + '</li>'
    resp+='<li>param_id: ' + str(param_id) + '</li>'
    resp+='</ul>'
    return HttpResponse(resp)
...
```


* Шаблон
Шаблон "web_param/templates/web_param/param.html" для обработки переданного
контекста из представления web_param/views.py:param_context



### Авторизация в Django                                 <a name='django-auth'></a>
Один из вариантов авторизации на сайте

Возможные варианты решения задачи авторизации в Django:
* Используем то что есть
* proxy - Простое расширение существующей модели
* один-к-одному - Новая модель дополняющая моедль User Profiles
* Наследование от AbstractBaseUser
* Наследование от AbstractUser

Ссылки:
* https://www.youtube.com/watch?v=dBctY3-Z5hY&list=TLPQMjAwMTIwMjAL3J9EEh89eA&index=4
* https://docs.djangoproject.com/en/3.0/topics/auth/default/#limiting-access-to-logged-in-users
* https://docs.djangoproject.com/en/3.0/topics/auth/
* https://habr.com/ru/post/313764/

Словарь:
* django.contrib.auth.forms, UserCreationForm
* django.contrib.auth, authenticate, login
* request.user.is_authenticated
* request.user.is_staff
* django.contrib.auth.forms,  UserCreateionForm
* LOGIN_REDIRECT_URL

Пример реализации:
* [auth](/examples/django/auth.zip)





### Отладка в Django                                     <a name='django-debug'></a>
Самый простейший способ отладки, это печать в sys.stderror

Пример:
```
import sys
from pprint import pprint
...
def register(request):
    pprint("Register view start!", sys.stderr)
    pprint(request, sys.stderr)
```
Пример реализации:
* [debug-print](/examples/django/debug-print.zip)



### Работа с шаблонами в Django                          <a name='django-templates'></a>
* Включение блока в шаблон
см.: [model.zip](/examples/django/model.zip)
```
    {% include "webapp/debug.html" %}
```
### Задача 1: Ссылка на приложение (urls.py)

Включить ссылки приложения "webapp" в основной файл ссылок проекта "webengine"

Решение:

* Изменения в файле webengine\settings.py
```
...
INSTALLED_APPS = [
    'webapp',
...
```
* Изменения в файле webengine\urls.py
```
from django.urls import path, include
...
urlpatterns = [
...
    path ('', include(webapp.urls')),
...
]
```
* Убедится, что файл webapp/urls.py существует

Проект с решением:
* [model.zip](/examples/django/model.zip)





### Задача 2: Перенаправление со страницы (redirect)
Добавить перенаправление (redirect) со страницы '/' на страницу '/some'.
В качестве представления использовать index из webapp
* Имя приложения: webapp
* Имя представления в webapp: index
* адрес перенаправления (откуда): ''
* адрес перенаправления (куда): '/some'

Для решения использовать проект: [model.zip](/examples/django/model.zip)

Ссылки:
* https://www.youtube.com/watch?v=QC3eQe_2uv8&list=TLPQMjAwMTIwMjAL3J9EEh89eA&index=5
* https://docs.djangoproject.com/en/3.0/topics/http/shortcuts/

Решение:


* Добавить представление в файл проекта webengine\views.py
```
...
from django.shortcuts import redirect
...
def some_redir(request):
    return redirect ('some_list_url', permanent=True)
...
```


* Изменения в файле webengine\urls.py
```
...
from . import views
...
urlpatterns = [
...
    path('', views.some_redir),
...
]
```


* Изменения в файле webapp\urls.py
```
...
from . import views
...
urlpatterns = [
...
       path ('some/', views.index, name='some_list_url'),
...
]
...

```

Проект с решением:
* [redirect.zip](/examples/django/redirect.zip)




### Задача 3: Вывод отладочной информации
Из представления index напечатать в окно отладочного сервера django сообщение.
* сообщение: view Index is started
* приложение: webapp
* представление: index
Для решения использовать проект: [model.zip](/examples/django/model.zip)

Решение:
* файл webapp\views.py
```
...
import sys
from pprint import pprint
...
def index(request):
...
    pprint("view Index is started", sys.stderr)
...
```

Проект с решением:
* [debug-print.zip](/examples/django/debug-print.zip)



## Предельтно простой веб сервер                                       <a name='http-server'></a>
Внезапно, в Питоне уже есть средства для построения самого простого веб
сервера.  В примере ниже, сервер запустится на порту 8888 и в ответ на
обращение выдаст простую страницу.

* Версия питона
```
python --version
> Python 3.6.3 :: Anaconda, Inc.
```

* web-server.py
```
from http.server import BaseHTTPRequestHandler, HTTPServer

class web_server(BaseHTTPRequestHandler):
 
    def do_GET(self):
        if self.path == '/':
            self.path = '/index.html'
        try:
            #Reading the file
            file_to_open = open(self.path[1:]).read()
            self.send_response(200)
        except:
            file_to_open = "File not found"
            self.send_response(404)
        
        self.end_headers()
        self.wfile.write(bytes(file_to_open, 'utf-8'))
	
httpd = HTTPServer(('localhost', 8888), web_server)
httpd.serve_forever()
```
* Страница index.html
```
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Просто страница</title>
    </head>
    <body>
        <h1>Index page!</h1>
    </body>
</html>
```
* Запуск
```
python web-server.py
```



## Pandas (Примеры)                                                    <a name='pandas-example'></a>

### Построчная обработка набора

* Через функцию
```
### Создание набора
data = {"a":[1, 11, 111], "b": [2, 22, 222], "c": [3, 33, 333]}
df = pd.DataFrame(data)

def abc(a):
    return (str(a.to_json()))

df['jsn'] = df.apply(abc, axis=1)
print (df)
```

* То-же но через lambda
```
data = {"a":[1, 11, 111], "b": [2, 22, 222], "c": [3, 33, 333]}
df = pd.DataFrame(data)
df['jsn'] = df.apply(lambda a: str(a.to_json()), axis=1)
print (df)
```



## Python Fizz Buzz                                                    <a name='python-fbz'></a>

* Блокнот с примерами доступен по [ссылке](/examples/python/python-basics.ipynb).
* Блокнот с задачами доступен  по [ссылке](/examples/python/python-basics-task.ipynb).

### Задача Fizz Buzz

Напишите программу, которая выводит на экран числа от 1 до 100. При этом
вместо чисел, кратных трем, программа должна выводить слово Fizz, а вместо
чисел, кратных пяти — слово Buzz. Если число кратно пятнадцати, то
программа должна выводить слово FizzBuzz

Решение:

```
for i in range(1,100):
    s = ''
    if i%3==0:
        s = 'Fizz'
    if i%5==0:
        s += 'Buzz'
    if s=='':
        s = str(i)
    print (i, s)
```
